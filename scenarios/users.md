## Users

common story for users

### Auth

- As a user I want to login using my credentials so I can use the service
- When I forgot my password, I want to reset my current password so I can use my new password to login
- When I use wrong credentials, I need to know which one is incorrect so I can determine
  all of my combination
